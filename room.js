class Room {
    constructor({id, background, backgroundColor, headerText, preset, currentDrawData}) {
        this.id = id;
        this.preset = preset;
        this.users = {};
        this.background = background;
        this.backgroundColor = backgroundColor;
        this.headerText = headerText ? headerText : "Chào mừng đến với room " + id;
        this.currentDrawData = null;
    }

    addUser(user) {
        // console.log(this.users);
        this.broadCast('user_join', user.getInfo());
        this.users[user.id] = user;
        user.setRoom(this);
        user.initUserList(this.getAllUserInfo());
        user.updateRoom(this.getInfo());
    }

    removeUser(user) {
        this.users[user.id].lastMessage = "";
        delete this.users[user.id];
        this.broadCast('user_leave', user.id);
    }

    getInfo() {
        return {
            id: this.id,
            background: this.background,
            backgroundColor: this.backgroundColor,
            headerText: this.headerText,
            currentDrawData: this.currentDrawData,
        }
    }

    getAllUserInfo() {
        let ret = {};
        for(let id in this.users) {
            ret[id] = this.users[id].getInfo();
        }
        return ret;
    }

    broadCast(channel, msg, excludeIds) {
        for(let id in this.users) {
            if(excludeIds && excludeIds.includes(id)) {
                continue;
            }
            // console.log("send to user: ", id, channel, msg);
            this.users[id].getSock().emit(channel, msg);
        }
    }
}

module.exports = Room