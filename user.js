class User {
    constructor({socket, avatar, nickName, color, voicePitch}) {
        this.room = undefined;
        this.id = socket.id;
        this.socket = socket;
        this.avatar = avatar;
        this.nickName = nickName;
        this.color = color;
        this.voicePitch = voicePitch;
        this.lastMessage = "";
        this.position = {x: 0, y: 1, z: 1}
        this.lookAt = {x: 0, y: 0, z: 0}
        this.bindEvent();
    }

    getSock() {
        return this.socket;
    }

    setRoom(room) {
        this.room = room;
    }

    setAvatar(avatar) {
        this.avatar = avatar
    }

    notifyChangeInfo() {
        this.room && this.room.broadCast('update_info', this.getInfo());
    }

    getInfo() {
        return {
            id: this.id,
            avatar: this.avatar,
            nickName: this.nickName,
            color: this.color,
            voicePitch: this.voicePitch,
            lastMessage: this.lastMessage,
            position: this.position,
            lookAt: this.lookAt,
        }
    }

    chatAll(msg) {
        const message = {
            userId: this.id,
            message: msg
        }
        this.lastMessage = msg;
        this.room && this.room.broadCast('user_chat', message);
    }

    broadCastVoice(data) {
        this.room && this.room.broadCast('voice_data', data, [this.id]);
    }

    broadCastDraw(data) {
        if(this.room){
            this.room.broadCast('draw_data', data, [this.id]);
            this.room.currentDrawData = data != "c" ? data : null;
        } 
    }

    broadCastStopWatchVideo(data) {
        this.room && this.room.broadCast('watch_video_stop', data, [this.id]);
    }

    initUserList(users) {
        this.socket.emit('init_user_list', users)
    }

    updateRoom(room) {
        this.socket.emit('update_room', room)
    }

    updateInfo(data) {
        const allow = ['avatar', 'nickName', 'color', 'voicePitch', 'position', 'lookAt'];
        for(let i = 0; i < allow.length; i++) {
            const property = allow[i];
            if(data[property]) {
                this[property] = data[property];
            }
        }
        this.notifyChangeInfo();
    }

    updateRoomConfig(data) {
        if(!this.room || this.room.preset) return;
        const allow = ['background', 'headerText', 'backgroundColor'];
        for(let i = 0; i < allow.length; i++) {
            const property = allow[i];
            if(data[property] != undefined) {
                this.room[property] = data[property];
            }
        }
        this.room.broadCast('update_room', this.room.getInfo());
    }

    bindEvent() {
        this.socket.on('disconnect', () => {
            // console.log('user disconnected');
            this.room && this.room.removeUser(this);
        });

        this.socket.on('user_chat', (msg) => {
            // console.log('message: ' + msg);
            this.chatAll(msg);
        });

        this.socket.on('update_info', (data) => {
            // console.log('update_info', data);
            this.updateInfo(data);
        });

        this.socket.on('update_room_config', (data) => {
            // console.log('update_room_config', data);
            this.updateRoomConfig(data);
        });

        this.socket.on('voice_data', (data) => {
            // console.log('voice_data', data);
            this.broadCastVoice(data);
        });

        this.socket.on('draw_data', (data) => {
            this.broadCastDraw(data);
        });

        this.socket.on('watch_video_stop', (data) => {
            this.broadCastStopWatchVideo(data);
        });
    }
}

module.exports = User