const express = require('express');
const app = express();
app.use(express.static('public'));

const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const User = require('./user');
const Room = require('./room');

let roomList = {
    "1": new Room({
        id: 1,
        preset: true,
        background: "https://media.moitruong.net.vn/2021/02/mua-xuan-hanh-phuc.jpg",
        headerText: "Preset room - Mùa xuân ấm áp"
    }),
    "2": new Room({
        id: 2,
        preset: true,
        background: "https://vtv1.mediacdn.vn/thumb_w/650/Uploaded/nguyenngan/2013_05_09/le-hoi-hp1_635037041109294341.jpg",
        headerText: "Preset room - Mùa hè nóng chảy mỡ"
    }),
    "3": new Room({
        id: 3,
        preset: true,
        background: "https://vnkings.com/wp-content/uploads/2018/04/14_Central_Park_1.jpg",
        headerText: "Preset room - Mùa thu đáng yêu"
    }),
    "4": new Room({
        id: 3,
        preset: true,
        background: "https://vnkings.com/wp-content/uploads/2018/03/maxresdefault-2.jpg",
        headerText: "Preset room - Mùa đông ối zồi ôi"
    }),
};

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    console.log('a user connected');
    let user = new User({socket});

    socket.on('join_room', (id) => {
        user.room && user.room.removeUser(user);
        console.log('user join room', user.id, id);

        if(roomList[id]){
            roomList[id].addUser(user);
        } else {
            let room = new Room({id});
            roomList[id] = room;
            room.addUser(user);
        }
    });
});

server.listen(3030, () => {
    console.log('listening on *:3030');
});