import * as THREE from './three/three.module.js';
import { AnhLDFirstPersonControls } from "./three/controls/AnhLDFirstPersonControls.js";
import Stats from "./three/libs/stats.module.js";
import { InteractionManager } from "./three/libs/three.interactive.module.js";
import { PositionalAudioHelper } from './three/helpers/PositionalAudioHelper.js';
import { MyCube } from "./three/objects/MyCube.js";
import { Wall } from './three/objects/Wall.js';


class World3D {
    constructor() {
        this.sceneObjects = [];
        this.interactionObjects = [];
        this.users = {};
        this.userFirstInits = {};
        this.user3Ds = {};
        this.clock = new THREE.Clock();
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.cameraPositionListenerCallbacks = [];
        this.lastCameraPosition = {
            x: this.camera.position.x,
            y: this.camera.position.y,
            z: this.camera.position.z,
        };

        this.queuePosition = {};
        this.queueMove = {};

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        document.body.appendChild(this.renderer.domElement);

        this.stats = new Stats();
        this.stats.dom.classList.add('threejs-stats');
        document.body.appendChild(this.stats.dom);

        this.controls = new AnhLDFirstPersonControls(this.camera, this.renderer.domElement);
        this.controls.movementSpeed = 3;

        const grid = new THREE.GridHelper(50, 50, 0x888888, 0x888888);
        this.scene.add(grid);

        this.interactionManager = new InteractionManager(
            this.renderer,
            this.camera,
            this.renderer.domElement
        );

        this.audioListener = new THREE.AudioListener();
        this.camera.add(this.audioListener);
    }

    animate() {
        requestAnimationFrame(() => { this.animate(); });
        this.controls.update(this.clock.getDelta());
        this.stats.update();
        this.interactionManager.update();
        this.updateMainUserPosition();
        this.handleCameraPositionCallbacks();
        this.updateMoveThing();
        this.renderer.render(this.scene, this.camera);
    }

    addCameraPositionListenerCallbacks(cb) {
        this.cameraPositionListenerCallbacks.push(cb);
    }

    handleCameraPositionCallbacks() {
        if (Math.abs(this.lastCameraPosition.x - this.camera.position.x) > 0.5
            || Math.abs(this.lastCameraPosition.y - this.camera.position.y) > 0.5
            || Math.abs(this.lastCameraPosition.z - this.camera.position.z) > 0.5
        ) {
            this.cameraPositionListenerCallbacks.forEach((cb) => {
                cb({
                    x: this.camera.position.x,
                    y: this.camera.position.y,
                    z: this.camera.position.z,
                }, {
                    x: this.controls.lookVector.x,
                    y: this.controls.lookVector.y,
                    z: this.controls.lookVector.z
                })
            });

            this.lastCameraPosition = {
                x: this.camera.position.x,
                y: this.camera.position.y,
                z: this.camera.position.z,
            };
        }

    }

    addUser(user) {
        if (user.id == this.userId) {
            return;
        }

        this.users[user.id] = user;
        this.userFirstInits[user.id] = true;
        const color = user.color || "#000000";
        const userTexture = new THREE.TextureLoader().load(user.avatar ? user.avatar : '/img/blank-avatar.png');
        const userTextureOther = new THREE.TextureLoader().load('/img/textures/crate.gif');
        const user3d = new THREE.Mesh(new THREE.BoxGeometry(0.3, 0.3, 0.3), [
            new THREE.MeshBasicMaterial({ color: color }),
            new THREE.MeshBasicMaterial({ color: color }),
            new THREE.MeshBasicMaterial({ color: color }),
            new THREE.MeshBasicMaterial({ color: color }),
            new THREE.MeshBasicMaterial({ map: userTexture }),
            new THREE.MeshBasicMaterial({ color: color }),
        ]);
        user3d.position.set(user.position.x, user.position.y, user.position.z);
        user3d.castShadow = true;
        
        // Setup voice
        // const voice = new THREE.PositionalAudio(this.audioListener);
        // const audioLoader = new THREE.AudioLoader();
        // audioLoader.load('sounds/bg.ogg', function (buffer) {
        //     voice.setBuffer(buffer);
        //     voice.setRefDistance(20);
        //     voice.setDirectionalCone(180, 230, 0.1);
        // });
        // const helper = new PositionalAudioHelper(voice);
        // voice.add(helper);
        // user3d.add(voice);
        
        this.scene.add(user3d);
        this.sceneObjects.push(user3d);
        this.queuePosition[user.id] = [];
        this.queueMove[user.id] = [];
        this.user3Ds[user.id] = user3d;

        
    }

    updateMoveThing() {
        for (let userId in this.queueMove) {

            if (this.queueMove[userId].length == 0) {
                const qPosition = this.queuePosition[userId].shift();
                if (qPosition) {
                    const dx = qPosition.x - this.user3Ds[userId].position.x;
                    const dy = qPosition.y - this.user3Ds[userId].position.y;
                    const dz = qPosition.z - this.user3Ds[userId].position.z;

                    const smooth = 10;

                    if (this.user3Ds[userId]) {
                        let tmp = {
                            x: this.user3Ds[userId].position.x,
                            y: this.user3Ds[userId].position.y,
                            z: this.user3Ds[userId].position.z,
                        }

                        for (let i = 0; i < smooth; i++) {
                            tmp.x += dx / smooth;
                            tmp.y += dy / smooth;
                            tmp.z += dz / smooth;

                            this.queueMove[userId].push({
                                x: tmp.x,
                                y: tmp.y,
                                z: tmp.z,
                            })
                        }
                    }
                }
            }

            const newPosition = this.queueMove[userId].shift();
            if (newPosition) {
                this.user3Ds[userId].position.x = newPosition.x;
                this.user3Ds[userId].position.y = newPosition.y;
                this.user3Ds[userId].position.z = newPosition.z;
            }
        }
    }

    removeUser(user) {
        if (this.user3Ds[user.id]) {
            this.scene.remove(this.user3Ds[user.id]);
            delete this.sceneObjects[user.id];
            delete this.user3Ds[user.id];
            delete this.users[user.id];
        }
    }

    updateUser(user) {
        if (user.id == this.userId) {
            return;
        }
        this.users[user.id] = user;
        if (this.userFirstInits[user.id] == true) {
            if (this.user3Ds[user.id]) {
                this.user3Ds[user.id].position.x = user.position.x;
                this.user3Ds[user.id].position.y = user.position.y;
                this.user3Ds[user.id].position.z = user.position.z;
            }
            this.user3Ds[user.id].lookAt(new THREE.Vector3(user.lookAt.x, user.lookAt.y, user.lookAt.z));
            delete this.userFirstInits[user.id];
        }

        this.queuePosition[user.id].push(user.position);
        this.user3Ds[user.id].lookAt(new THREE.Vector3(user.lookAt.x, user.lookAt.y, user.lookAt.z));
    }

    updateMainUserPosition() {
        if (this.userId && this.user3Ds[this.userId]) {
            this.user3Ds[this.userId].position.x = this.camera.position.x;
            this.user3Ds[this.userId].position.y = this.camera.position.y;
            this.user3Ds[this.userId].position.z = this.camera.position.z;
        }
    }

    createRoom({ userId }) {
        this.userId = userId;
        this.camera.position.z = 10;
        // this.scene.background = new THREE.Color( 0xffffff );

        const wall = new Wall({ mode: 0, width: 15, height: 20 });
        this.scene.add(wall);
        this.sceneObjects.push(wall);

        const pictureTexture = new THREE.TextureLoader().load('/img/pictures/spring.jpeg')
        const pictureMaterial = new THREE.MeshStandardMaterial({ map: pictureTexture });
        const picturePlane = new THREE.PlaneGeometry(2, 1.5);
        const pictureMesh = new THREE.Mesh(picturePlane, pictureMaterial);
        pictureMesh.position.x += 4.2;
        pictureMesh.position.y += 1.5;
        pictureMesh.position.z += 0.05;
        pictureMesh.receiveShadow = true;
        this.scene.add(pictureMesh);
        this.sceneObjects.push(pictureMesh);

        const groundTexture = new THREE.TextureLoader().load('/img/textures/ground.jpeg');
        const groundMaterial = new THREE.MeshStandardMaterial({ map: groundTexture, side: THREE.DoubleSide });
        const groundPlane = new THREE.PlaneGeometry(20, 20);
        const groundMesh = new THREE.Mesh(groundPlane, groundMaterial);
        groundMesh.rotateX(Math.PI / 2);
        groundMesh.receiveShadow = true;
        this.scene.add(groundMesh);
        this.sceneObjects.push(groundMesh);

        const cubeMusicTexture = new THREE.TextureLoader().load('/img/textures/crate.gif');
        const cubeMusicGeometry = new THREE.BoxGeometry();
        const cubeMusicMaterial = new THREE.MeshStandardMaterial({ map: cubeMusicTexture });
        const cubeMusic = new THREE.Mesh(cubeMusicGeometry, cubeMusicMaterial);
        cubeMusic.castShadow = true;
        cubeMusic.position.x = -2;
        cubeMusic.position.y = 0.5;
        cubeMusic.position.z = 2;
        this.scene.add(cubeMusic);
        this.sceneObjects.push(cubeMusic);

        // Setup sound
        const sound = new THREE.PositionalAudio(this.audioListener);
        const audioLoader = new THREE.AudioLoader();
        audioLoader.load('sounds/bg.ogg', function (buffer) {
            sound.setBuffer(buffer);
            sound.setRefDistance(20);
            sound.setDirectionalCone(180, 230, 0.1);
        });
        // const helper = new PositionalAudioHelper(sound);
        // sound.add(helper);
        cubeMusic.add(sound);

        ['click', 'touchstart'].forEach((action) => {
            cubeMusic.addEventListener(action, (e) => {
                sound.play();
            });
        });

        ['mouseover'].forEach((action) => {
            cubeMusic.addEventListener(action, (event) => {
                event.target.material.color.set(0xff0000);
                document.body.style.cursor = "pointer";
            });
        });

        ['mouseout'].forEach((action) => {
            cubeMusic.addEventListener(action, (event) => {
                event.target.material.color.set(0xffffff);
                document.body.style.cursor = "default";
            });
        });

        this.interactionManager.add(cubeMusic);
        this.interactionObjects.push(cubeMusic);

        // Setup light
        const spotLight = new THREE.SpotLight(0xffffff, 2);
        spotLight.position.set(5, 10, 10);
        spotLight.castShadow = true;
        this.scene.add(spotLight);
        this.sceneObjects.push(spotLight);
        const sun = new THREE.Mesh(new THREE.SphereGeometry(0.5), new THREE.MeshBasicMaterial({ color: 0xffffff }));
        sun.position.set(5, 10, 10);
        this.scene.add(sun);
        this.sceneObjects.push(sun);
    }

    destroyRoom() {
        this.sceneObjects.forEach((obj) => {
            this.scene.remove(obj);
        });

        this.interactionObjects.forEach((obj) => {
            this.interactionManager.remove(obj);
        })
    }
}

export { World3D };



// const clock = new THREE.Clock();
// const scene = new THREE.Scene();
// const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

// const renderer = new THREE.WebGLRenderer();
// renderer.setSize(window.innerWidth, window.innerHeight);
// renderer.shadowMap.enabled = true;
// renderer.shadowMap.type = THREE.PCFSoftShadowMap;
// document.body.appendChild(renderer.domElement);

// const plane1 = new THREE.PlaneGeometry(3, 5);
// const plane2 = new THREE.PlaneGeometry(10, 10);
// const textureP = new THREE.TextureLoader().load('/img/textures/w2.jpeg');
// const materialP = new THREE.MeshStandardMaterial({ map: textureP, side: THREE.DoubleSide });
// const wallFace = new THREE.Mesh(plane1, materialP);
// const wallFace2 = new THREE.Mesh(plane2, materialP);
// // scene.add(wallFace);
// wallFace2.receiveShadow = true;
// // wallFace2.castShadow = true;
// scene.add(wallFace2);
// wallFace2.position.z += 1
// wallFace2.rotateX(Math.PI / 2);


// const wall = new Wall({ mode: 0 });
// // wall.castShadow = true;
// const wall2 = new Wall({ mode: 1 });

// scene.add(wall);
// scene.add(wall2);

// wall.position.z = 2;
// wall2.position.z = 2 - 0.2;
// wall2.rotateY(-Math.PI / 2);

// const planeGeometry = new THREE.PlaneGeometry(20, 20, 32, 32);
// const planeMaterial = new THREE.MeshStandardMaterial({ color: 0x00ff00, side: THREE.DoubleSide })
// const plane = new THREE.Mesh(planeGeometry, planeMaterial);
// plane.receiveShadow = true;
// scene.add(plane);

// const texture = new THREE.TextureLoader().load('/img/textures/crate.gif');
// const texture2 = new THREE.TextureLoader().load('/img/textures/w2.jpeg');
// const geometry = new THREE.BoxGeometry();
// const material = new THREE.MeshStandardMaterial({ map: texture });
// const material2 = new THREE.MeshStandardMaterial({ map: texture2 });
// const cube = new THREE.Mesh(geometry, material);
// const cube2 = new THREE.Mesh(geometry, material2);
// const cube3 = new THREE.Mesh(geometry, material);
// cube2.position.z = -5;
// cube3.position.y = 0.5;
// cube3.position.x = -2;
// cube3.position.z = 2;
// cube3.castShadow = true;
// cube3.receiveShadow = true;
// // scene.add(cube);
// // scene.add(cube2);
// // scene.add(cube3);
// camera.position.z = 10;
// camera.position.y = 1;
// const controls = new AnhLDFirstPersonControls(camera, renderer.domElement);
// controls.movementSpeed = 3;

// const geometryX = new THREE.BufferGeometry().setFromPoints([camera.position, new THREE.Vector3(100, camera.position.y, camera.position.z)]);
// const geometryY = new THREE.BufferGeometry().setFromPoints([camera.position, new THREE.Vector3(camera.position.x, 100, camera.position.z)]);
// const geometryZ = new THREE.BufferGeometry().setFromPoints([camera.position, controls.lookVector]);
// const materialX = new THREE.LineBasicMaterial({ color: "#32a852" });
// const materialY = new THREE.LineBasicMaterial({ color: "#329aa8" });
// const materialZ = new THREE.LineBasicMaterial({ color: "#a83294" });
// const lineX = new THREE.Line(geometryX, materialX);
// const lineY = new THREE.Line(geometryY, materialY);
// const lineZ = new THREE.Line(geometryZ, materialZ);
// scene.add(lineX);
// scene.add(lineY);
// scene.add(lineZ);

// const grid = new THREE.GridHelper(50, 50, 0x888888, 0x888888);
// scene.add(grid);

// const skyColor = 0xB1E1FF;  // light blue
// const groundColor = 0xB97A20;  // brownish orange
// const intensity = 1;
// const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
// scene.add(light);

// const spotLight = new THREE.SpotLight(0xffffff, 2);
// spotLight.position.set(5, 10, 10);
// spotLight.castShadow = true;

// scene.add(spotLight);

// const sphere = new THREE.Mesh(new THREE.SphereGeometry(0.5), new THREE.MeshBasicMaterial({ color: 0xffffff }));
// scene.add(sphere);
// sphere.position.set(5, 10, 10);



// // create an AudioListener and add it to the camera
// const listener = new THREE.AudioListener();
// camera.add(listener);

// // create the PositionalAudio object (passing in the listener)
// const sound = new THREE.PositionalAudio(listener);

// // load a sound and set it as the PositionalAudio object's buffer
// const audioLoader = new THREE.AudioLoader();
// audioLoader.load('sounds/bg.ogg', function (buffer) {
//     sound.setBuffer(buffer);
//     sound.setRefDistance(20);
//     sound.setDirectionalCone(180, 230, 0.1);
// });

// const helper = new PositionalAudioHelper(sound, 0.1);
// sound.add(helper);


// cube3.add(sound);

// scene.add(cube3);

// const stats = new Stats();
// document.body.appendChild(stats.dom);

// const interactionManager = new InteractionManager(
//     renderer,
//     camera,
//     renderer.domElement
// );

// ['click', 'touchstart'].forEach((action) => {
//     cube3.addEventListener(action, (e) => {
//         console.log('OKOK');
//         sound.play();
//     });
// });


// ['mouseover'].forEach((action) => {
//     cube3.addEventListener(action, (event) => {
//         event.target.material.color.set(0xff0000);
//         document.body.style.cursor = "pointer";
//     });
// });

// ['mouseout'].forEach((action) => {
//     cube3.addEventListener(action, (event) => {
//         event.target.material.color.set(0xffffff);
//         document.body.style.cursor = "default";
//     });
// });

// interactionManager.add(cube3);

// const animate = () => {
//     // console.log(cube.position);
//     requestAnimationFrame(animate);
//     controls.update(clock.getDelta());
//     stats.update();
//     interactionManager.update();
//     // spotLight.position.y -= 0.01;
//     renderer.render(scene, camera);
// }

// const onWindowResize = () => {
//     camera.aspect = window.innerWidth / window.innerHeight;
//     camera.updateProjectionMatrix();
//     renderer.setSize(window.innerWidth, window.innerHeight);
// }

// const log = (text) => {
//     document.getElementById('log').innerHTML = text;
// }

// window.addEventListener('resize', onWindowResize, false);

// animate();