import * as THREE from '../three.module.js';
import { MyCube } from './MyCube.js'
class Wall {
    constructor({ mode, width, height }) {
        this.mode = mode || 0;
        this.width = width || 10;
        this.height = height || 10;
        const wall = new THREE.Group();
        let dY = 0;
        for (let j = 0; j < this.height; j++) {
            const chage1 = this.mode == 0 ? 0 : 0.4 / 2;
            const chage2 = this.mode == 1 ? 0 : 0.4 / 2;
            const dx = j % 2 == 0 ? chage1 : chage2;
            for (let i = 0; i < this.width; i++) {
                const brick = new MyCube({ width: 0.4, height: 0.15, thick: 0.2 });
                brick.castShadow = true;
                brick.receiveShadow = true;
                brick.position.x += i * 0.4 + dx;
                brick.position.y += dY;

                if (i > 3 && i < 6) {
                    continue;
                }

                wall.add(brick);
            }
            dY += 0.15;
        }

        return wall;
    }
}

export { Wall };