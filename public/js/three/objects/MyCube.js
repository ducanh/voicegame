import * as THREE from '../three.module.js';
class MyCube {
    constructor({
        width, height, thick
    }) {
        this.width = width;
        this.height = height;
        this.thick = thick;
        const geometry = new THREE.BufferGeometry();
        const imageSize = { width: this.width, height: this.height, thick: this.thick };
        const coords = { x: 0, y: 0, z: 0 };


        const verticesRaw = [];
        const face1 = [
            coords.x, coords.y, coords.z, // bottom left
            coords.x + imageSize.width, coords.y, coords.z, // bottom right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z, // upper right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z, // upper right
            coords.x, coords.y + imageSize.height, coords.z, // upper left
            coords.x, coords.y, coords.z, // bottom left
        ];

        const face2 = [
            coords.x, coords.y, coords.z - imageSize.thick, // bottom left
            coords.x, coords.y, coords.z, // bottom right
            coords.x, coords.y + imageSize.height, coords.z, // upper right
            coords.x, coords.y + imageSize.height, coords.z, // upper right
            coords.x, coords.y + imageSize.height, coords.z - imageSize.thick, // upper left
            coords.x, coords.y, coords.z - imageSize.thick, // bottom left
        ];

        const face3 = [
            coords.x + imageSize.width, coords.y, coords.z - imageSize.thick, // bottom left
            coords.x, coords.y, coords.z - imageSize.thick, // bottom right
            coords.x, coords.y + imageSize.height, coords.z - imageSize.thick, //upper right
            coords.x, coords.y + imageSize.height, coords.z - imageSize.thick, //upper right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z - imageSize.thick, // upper left
            coords.x + imageSize.width, coords.y, coords.z - imageSize.thick, // bottom left

        ];

        const face4 = [
            coords.x + imageSize.width, coords.y, coords.z, // bottom left
            coords.x + imageSize.width, coords.y, coords.z - imageSize.thick, // bottom right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z - imageSize.thick, // upper right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z - imageSize.thick, // upper right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z, // upper left
            coords.x + imageSize.width, coords.y, coords.z, // bottom left
        ];

        const face5 = [
            coords.x, coords.y + imageSize.height, coords.z, // bottom left
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z, // bottom right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z - imageSize.thick, // upper right
            coords.x + imageSize.width, coords.y + imageSize.height, coords.z - imageSize.thick, // upper right
            coords.x, coords.y + imageSize.height, coords.z - imageSize.thick, // upper left
            coords.x, coords.y + imageSize.height, coords.z, // bottom left
        ];

        const face6 = [
            coords.x + imageSize.width, coords.y, coords.z, // bottom left
            coords.x, coords.y, coords.z, // bottom right
            coords.x, coords.y, coords.z - imageSize.thick, // upper right
            coords.x, coords.y, coords.z - imageSize.thick, // upper right
            coords.x + imageSize.width, coords.y, coords.z - imageSize.thick, // upper left
            coords.x + imageSize.width, coords.y, coords.z, // bottom left
        ];

        verticesRaw.push(...face1);
        verticesRaw.push(...face2);
        verticesRaw.push(...face3);
        verticesRaw.push(...face4);
        verticesRaw.push(...face5);
        verticesRaw.push(...face6);
        const vertices = new Float32Array(verticesRaw);

        const uvsRaw = [];
        for (let i = 0; i < 6; i++) {
            uvsRaw.push(...[0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                1.0, 1.0,
                0.0, 1.0,
                0.0, 0.0,]
            );
        }

        const uvs = new Float32Array(uvsRaw);

        geometry.setAttribute('uv', new THREE.BufferAttribute(uvs, 2));
        geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
        geometry.computeVertexNormals();

        const textureP = new THREE.TextureLoader().load('/img/textures/w2.jpeg');
        const textureP2 = new THREE.TextureLoader().load('/img/textures/crate.gif');
        // const material = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        const material = new THREE.MeshStandardMaterial({ map: textureP });
        const material2 = new THREE.MeshLambertMaterial({
            map: textureP2, 
        });
        geometry.addGroup(0, 6, 0);
        geometry.addGroup(6, 30, 1);
        this.mesh = new THREE.Mesh(geometry, [material2, material2]);

        return this.mesh;
    }
}

export { MyCube };