import {
    MathUtils,
    Object3D,
    Spherical,
    Vector3
} from '../three.module.js';

const _lookDirection = new Vector3();
const _spherical = new Spherical();
const _target = new Vector3();

const _xAxis = /*@__PURE__*/ new Vector3(1, 0, 0);
const _yAxis = /*@__PURE__*/ new Vector3(0, 1, 0);
const _zAxis = /*@__PURE__*/ new Vector3(0, 0, 1);

class AnhLDFirstPersonControls {
    constructor(camera, domElement) {

        if (domElement === undefined) {
            console.warn('THREE.FirstPersonControls: The second parameter "domElement" is now mandatory.');
            domElement = document;
        }

        this.camera = camera;
        this.lookVector = new Vector3(0, 1, -1000);
        this.domElement = domElement;

        this.movementSpeed = 1.0;
        this.lookSpeed = 0.05;
        this.verticalMin = 0;
        this.verticalMax = Math.PI;
        this.constrainVertical = false;
        this.lookVertical = true;

        this.horizontalAngle = Math.PI / 180;
        this.verticalAngle = Math.PI / 90;

        this.moveForward = false;
        this.moveBackward = false;
        this.moveLeft = false;
        this.moveRight = false;

        this.turnLeft = false;
        this.turnRight = false;
        this.turnUp = false;
        this.turnDown = false;

        this.viewHalfX = this.domElement.offsetWidth / 2;
        this.viewHalfY = this.domElement.offsetHeight / 2;

        this.mouseX = 0;
        this.mouseY = 0;

        let lat = 0;
        let lon = 0;

        this.update = function (delta) {
            const cameraX = this.camera.position.x;
            const cameraY = this.camera.position.y;
            const cameraZ = this.camera.position.z;

            if (this.camera.position.y < 2) {
                this.camera.position.y = 2;
            }

            const actualMoveSpeed = delta * this.movementSpeed;

            if (this.moveForward) {
                this.camera.translateZ(-actualMoveSpeed);
                _lookDirection.copy(_zAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(-actualMoveSpeed));
            };
            if (this.moveBackward) {
                this.camera.translateZ(actualMoveSpeed);
                _lookDirection.copy(_zAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(actualMoveSpeed));
            };

            if (this.moveLeft) {
                this.camera.translateX(-actualMoveSpeed);
                _lookDirection.copy(_xAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(-actualMoveSpeed));
            }
            if (this.moveRight) {
                this.camera.translateX(actualMoveSpeed);
                _lookDirection.copy(_xAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(actualMoveSpeed));
            }

            if (this.turnLeft) {
                const x = this.lookVector.x;
                const z = this.lookVector.z;

                const xNew = cameraX + (x - cameraX) * Math.cos(-this.horizontalAngle) - (z - cameraZ) * Math.sin(-this.horizontalAngle);
                const zNew = cameraZ + (x - cameraX) * Math.sin(-this.horizontalAngle) + (z - cameraZ) * Math.cos(-this.horizontalAngle);

                this.lookVector.setX(xNew);
                this.lookVector.setZ(zNew);
            }

            if (this.turnRight) {
                const x = this.lookVector.x;
                const z = this.lookVector.z;
                const xNew = cameraX + (x - cameraX) * Math.cos(this.horizontalAngle) - (z - cameraZ) * Math.sin(this.horizontalAngle);
                const zNew = cameraZ + (x - cameraX) * Math.sin(this.horizontalAngle) + (z - cameraZ) * Math.cos(this.horizontalAngle);
                this.lookVector.setX(xNew);
                this.lookVector.setZ(zNew);
            };

            if (this.turnDown) {
                const lookVectorQ = new Vector3();
                const cameraPositionQ = new Vector3();
                lookVectorQ.copy(this.lookVector);
                cameraPositionQ.copy(this.camera.position);

                lookVectorQ.applyQuaternion(this.camera.quaternion);
                cameraPositionQ.applyQuaternion(this.camera.quaternion);

                const y = lookVectorQ.y;
                const z = lookVectorQ.z;

                const cameraQY = cameraPositionQ.y;
                const cameraQZ = cameraPositionQ.z;

                const yNew = cameraQY + (y - cameraQY) * Math.cos(-this.verticalAngle) - (z - cameraQZ) * Math.sin(-this.verticalAngle);
                const zNew = cameraQZ + (y - cameraQY) * Math.sin(-this.verticalAngle) + (z - cameraQZ) * Math.cos(-this.verticalAngle);

                const dY = -Math.abs(yNew - y);
                const dZ = -Math.abs(zNew - z);

                _lookDirection.copy(_yAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(dY));
                _lookDirection.copy(_zAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(dZ));
            }

            if (this.turnUp) {
                const lookVectorQ = new Vector3();
                const cameraPositionQ = new Vector3();
                lookVectorQ.copy(this.lookVector);
                cameraPositionQ.copy(this.camera.position);

                lookVectorQ.applyQuaternion(this.camera.quaternion);
                cameraPositionQ.applyQuaternion(this.camera.quaternion);

                const y = lookVectorQ.y;
                const z = lookVectorQ.z;

                const cameraQY = cameraPositionQ.y;
                const cameraQZ = cameraPositionQ.z;

                const yNew = cameraQY + (y - cameraQY) * Math.cos(this.verticalAngle) - (z - cameraQZ) * Math.sin(this.verticalAngle);
                const zNew = cameraQZ + (y - cameraQY) * Math.sin(this.verticalAngle) + (z - cameraQZ) * Math.cos(this.verticalAngle);

                const dY = Math.abs(yNew - y);
                const dZ = Math.abs(zNew - z);

                _lookDirection.copy(_yAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(dY));
                _lookDirection.copy(_zAxis).applyQuaternion(this.camera.quaternion);
                this.lookVector.add(_lookDirection.multiplyScalar(dZ));
            };
            this.camera.lookAt(this.lookVector);

            // let log = "";
            // const cp = this.camera.position;
            // const lvp = this.lookVector;
            // log += "Camera: x: " + cp.x.toFixed(2) + " - y: " + cp.y.toFixed(2) + " - z: " + cp.z.toFixed(2) + "<br/>";
            // log += "Vector: x: " + lvp.x.toFixed(2) + " - y: " + lvp.y.toFixed(2) + " - z: " + lvp.z.toFixed(2) + "<br/>";
            // log += "Sub z: " + (lvp.z - cp.z).toFixed(2);

            // this.log(log);
        }

        this.onKeyDown = function (event) {

            switch (event.code) {

                case 'ArrowUp':
                case 'KeyW': this.moveForward = true; break;

                case 'ArrowLeft':
                case 'KeyA': this.moveLeft = true; break;

                case 'ArrowDown':
                case 'KeyS': this.moveBackward = true; break;

                case 'ArrowRight':
                case 'KeyD': this.moveRight = true; break;

                case 'KeyZ': this.turnLeft = true; break;
                case 'KeyC': this.turnRight = true; break;

                case 'KeyF': this.turnUp = true; break;
                case 'KeyV': this.turnDown = true; break;
            }

        };

        this.onKeyUp = function (event) {

            switch (event.code) {

                case 'ArrowUp':
                case 'KeyW': this.moveForward = false; break;

                case 'ArrowLeft':
                case 'KeyA': this.moveLeft = false; break;

                case 'ArrowDown':
                case 'KeyS': this.moveBackward = false; break;

                case 'ArrowRight':
                case 'KeyD': this.moveRight = false; break;

                case 'KeyZ': this.turnLeft = false; break;
                case 'KeyC': this.turnRight = false; break;

                case 'KeyF': this.turnUp = false; break;
                case 'KeyV': this.turnDown = false; break;

            }

        };

        this.onMouseMove = function (event) {
            this.mouseX = event.pageX - this.domElement.offsetLeft - this.viewHalfX;
            this.mouseY = event.pageY - this.domElement.offsetTop - this.viewHalfY;
        };

        this.log = function (text) {
            document.getElementById('log').innerHTML = text;
        }

        const _onMouseMove = this.onMouseMove.bind(this);
        const _onKeyDown = this.onKeyDown.bind(this);
        const _onKeyUp = this.onKeyUp.bind(this);

        this.domElement.addEventListener('mousemove', _onMouseMove);

        ['mousedown', 'touchstart'].forEach((action) => {
            document.getElementById('nav-forward').addEventListener(action, (e) => {
                this.moveForward = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-backward').addEventListener(action, (e) => {
                this.moveBackward = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-left').addEventListener(action, (e) => {
                this.moveLeft = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-right').addEventListener(action, (e) => {
                this.moveRight = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-up').addEventListener(action, (e) => {
                this.turnUp = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-down').addEventListener(action, (e) => {
                this.turnDown = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-turn-left').addEventListener(action, (e) => {
                this.turnLeft = true;
                e.target.classList.add('active');
                e.preventDefault();
            });

            document.getElementById('nav-turn-right').addEventListener(action, (e) => {
                this.turnRight = true;
                e.target.classList.add('active');
                e.preventDefault();
            });
        });

        ['mouseup', 'touchend'].forEach((action) => {
            document.getElementById('nav-forward').addEventListener(action, (e) => {
                this.moveForward = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-backward').addEventListener(action, (e) => {
                this.moveBackward = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-left').addEventListener(action, (e) => {
                this.moveLeft = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-right').addEventListener(action, (e) => {
                this.moveRight = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-up').addEventListener(action, (e) => {
                this.turnUp = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-down').addEventListener(action, (e) => {
                this.turnDown = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-turn-left').addEventListener(action, (e) => {
                this.turnLeft = false;
                e.target.classList.remove('active');
            });

            document.getElementById('nav-turn-right').addEventListener(action, (e) => {
                this.turnRight = false;
                e.target.classList.remove('active');
            });
        });

        const buttons = [...document.getElementsByTagName('button')];

        buttons.forEach((element) => {
            element.addEventListener('selectstart', (e) => {
                e.preventDefault();
                return false;
            });
        })

        window.addEventListener('keydown', _onKeyDown);
        window.addEventListener('keyup', _onKeyUp);
    }
}

export { AnhLDFirstPersonControls };